use std::str::FromStr;
use crate::Var;

#[cfg(target_arch = "wasm32")]
pub fn heinprimey(args: Vec<Var>) -> Var {
	let doc = web_sys::window().unwrap().document().unwrap();
	for a in args {
		let tn = doc.create_text_node(&format!("{}\n", a.str_repr()));
		doc.get_element_by_id("output").unwrap().append_child(&tn).ok();
	}

	Var::Void
}

#[cfg(not(target_arch = "wasm32"))]
pub fn heinprimey(args: Vec<Var>) -> Var {
	for a in args {
		println!("{}", a.str_repr());
	}

	Var::Void
}

pub fn aigualitet(args: Vec<Var>) -> Var {
	if let Some(first) = args.clone().into_iter().next() {
		Var::Bool(args.into_iter().fold((true, first), |(eq, prec), next| (eq && prec == next.clone(), next)).0)
	} else {
		Var::Bool(false)
	}
}

pub fn ferrand(args: Vec<Var>) -> Var {
	if let Some(first) = args.clone().into_iter().next() {
		Var::Bool(args.into_iter().fold((true, first), |(eq, prec), next| (eq && prec != next.clone(), next)).0)
	} else {
		Var::Bool(false)
	}
}

pub fn hainferilleur(args: Vec<Var>) -> Var {
	let mut args = args.into_iter().filter_map(|x| match x {
		Var::Int(x) => Some(x as f64),
		Var::Float(x) => Some(x),
		_ => None,
	});
	if let Some(first) = args.next().clone() {
		Var::Bool(args.fold((true, first), |(eq, prec), next| (eq && prec < next.clone(), next)).0)
	} else {
		Var::Bool(false)
	}
}

pub fn hainferilleur_houx_aigal(args: Vec<Var>) -> Var {
	let mut args = args.into_iter().filter_map(|x| match x {
		Var::Int(x) => Some(x as f64),
		Var::Float(x) => Some(x),
		_ => None,
	});
	if let Some(first) = args.next().clone() {
		Var::Bool(args.fold((true, first), |(eq, prec), next| (eq && prec <= next.clone(), next)).0)
	} else {
		Var::Bool(false)
	}
}

pub fn cu_perd_illeure(args: Vec<Var>) -> Var {
	let mut args = args.into_iter().filter_map(|x| match x {
		Var::Int(x) => Some(x as f64),
		Var::Float(x) => Some(x),
		_ => None,
	});
	if let Some(first) = args.next().clone() {
		Var::Bool(args.fold((true, first), |(eq, prec), next| (eq && prec > next.clone(), next)).0)
	} else {
		Var::Bool(false)
	}
}

pub fn cu_perd_illeure_houx_aigal(args: Vec<Var>) -> Var {
	let mut args = args.into_iter().filter_map(|x| match x {
		Var::Int(x) => Some(x as f64),
		Var::Float(x) => Some(x),
		_ => None,
	});
	if let Some(first) = args.next().clone() {
		Var::Bool(args.fold((true, first), |(eq, prec), next| (eq && prec >= next.clone(), next)).0)
	} else {
		Var::Bool(false)
	}
}

pub fn hageoutez(args: Vec<Var>) -> Var {
	let is_float = args.iter().any(|x| match x {
		Var::Float(_) => true,
		_ => false,
	});
	let args = args.into_iter().filter_map(|x| match x {
		Var::Int(x) => Some(x as f64),
		Var::Float(x) => Some(x),
		_ => None,
	});

	let res = args.fold(0.0, |s, next| s + next);
	if is_float {
		Var::Float(res)
	} else {
		Var::Int(res.floor() as i64)
	}
}

pub fn feranss(args: Vec<Var>) -> Var {
	let is_float = args.iter().any(|x| match x {
		Var::Float(_) => true,
		_ => false,
	});
	let args = args.into_iter().filter_map(|x| match x {
		Var::Int(x) => Some(x as f64),
		Var::Float(x) => Some(x),
		_ => None,
	});

	let res = args.fold(0.0, |s, next| s - next);
	if is_float {
		Var::Float(res)
	} else {
		Var::Int(res.floor() as i64)
	}
}

pub fn muletiplyer(args: Vec<Var>) -> Var {
	let is_float = args.iter().any(|x| match x {
		Var::Float(_) => true,
		_ => false,
	});
	let args = args.into_iter().filter_map(|x| match x {
		Var::Int(x) => Some(x as f64),
		Var::Float(x) => Some(x),
		_ => None,
	});

	let res = args.fold(1.0, |s, next| s * next);
	if is_float {
		Var::Float(res)
	} else {
		Var::Int(res.floor() as i64)
	}
}

pub fn vizet(args: Vec<Var>) -> Var {
	let is_float = args.iter().any(|x| match x {
		Var::Float(_) => true,
		_ => false,
	});
	let args: Vec<_> = args.into_iter().filter_map(|x| match x {
		Var::Int(x) => Some(x as f64),
		Var::Float(x) => Some(x),
		_ => None,
	}).collect();

	let res = args[1..].into_iter().fold(args[0], |s, next| s + next);
	if is_float {
		Var::Float(res)
	} else {
		Var::Int(res.floor() as i64)
	}
}

pub fn kaule(args: Vec<Var>) -> Var {
	Var::Str(args.into_iter().fold(String::new(), |s, x| format!("{}{}", s, x.str_repr())))
}

pub fn kaulavek(args: Vec<Var>) -> Var {
	let default = Var::Str(" ".into());
	let sep = args.clone().iter().next().clone().unwrap_or(&default).clone();
	Var::Str(args.into_iter().skip(1).fold(String::new(), |s, x| format!("{}{}{}", s, sep.str_repr(), x.str_repr())))
}

pub fn koup(args: Vec<Var>) -> Var {
	if let [Var::Str(ref sep), Var::Str(ref s)] = args[..2] {
		Var::List(s.clone().split(&sep[..]).map(|x| Var::Str(x.to_string())).collect())
	} else {
		Var::Void
	}
}

pub fn keskece(args: Vec<Var>) -> Var {
	if let Some(Var::Str(msg)) = args.into_iter().next() {
		Var::Str(web_sys::window().unwrap().prompt_with_message(&msg).unwrap_or_default().unwrap_or_default())
	} else {
		Var::Str(web_sys::window().unwrap().prompt().unwrap_or_default().unwrap_or_default())
	}
}

pub fn li_le_numaireau(args: Vec<Var>) -> Var {
	if let Some(Var::Str(s)) = args.into_iter().next() {
		Var::Int(i64::from_str(&s).unwrap_or(0))
	} else {
		Var::Void
	}
}

pub fn taye(args: Vec<Var>) -> Var {
	let a = args.into_iter().next();
	if let Some(Var::List(l)) = a {
		Var::Int(l.len() as i64)
	} else if let Some(Var::Dict(d)) = a {
		Var::Int(d.len() as i64)
	} else {
		Var::Void
	}
}

pub fn klai(args: Vec<Var>) -> Var {
	if let Some(Var::Dict(d)) = args.into_iter().next() {
		Var::List(d.keys().map(|x| Var::Str(x.clone())).collect())
	} else {
		Var::Void
	}
}
