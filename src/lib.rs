use wasm_bindgen::prelude::*;
use std::str::FromStr;
use std::collections::HashMap;

mod esse_te_de;

#[derive(Clone, Debug)]
pub enum Scalar {
    Bool(bool),
    Int(i64),
    Float(f64),
    Str(String),
    List(Vec<Expr>),
    Dict(HashMap<String, Expr>),
}

#[derive(Clone, Debug)]
pub enum Expr {
    Scalar(Scalar),
    Var(String),
    Call(String, Vec<Expr>),
    ListAccess(Box<Expr>, Box<Expr>),
}

#[derive(Clone, Debug)]
pub enum Ast {
	Root(Vec<Ast>),
    Decl(String, Expr),
    Call(String, Vec<Expr>),
    Func(String, Vec<String>, Vec<Ast>),
    Return(Expr),
    If(Vec<(Expr, Vec<Ast>)>),
    While(Expr, Vec<Ast>),
}

#[derive(Debug, PartialEq, Eq, Clone)]
enum Tok {
	Swa,
	Ident(String),
	Number(i64),
	Str(String),
	Equal,
	Dot,
	LPar,
	RPar,
	Comma,
	Colon,
	Fonkssion,
	Faim,
	Tank,
	Euh,
	Cy,
	C,
	Pa,
	Bon,
	Houx,
	Alore,
	Ranveauha,
	NewLine,
	Vre,
	Pho,
	LBrace,
	RBrace,
	LAcc,
	RAcc,
}

#[derive(Debug)]
enum Grammar {
	Main,
	Str,
	Comment
}

#[derive(Debug)]
struct Tokenizer {
	tokens: Vec<Tok>,
	chars: Vec<char>,
	gramm: Grammar,
}

impl Tokenizer {
	fn new(buf_size: usize) -> Tokenizer {
		Tokenizer {
			tokens: Vec::with_capacity(buf_size / 4),
			chars: Vec::with_capacity(20),
			gramm: Grammar::Main,
		}
	}
}

#[wasm_bindgen(start)]
pub fn main() {
	console_error_panic_hook::set_once();
	console_log::init_with_level(log::Level::Debug).unwrap();
}

#[wasm_bindgen]
pub fn execute(code: &str) {
	if let Err(e) = tok_parse_run(code) {
		esse_te_de::heinprimey(vec![Var::Str(match e {
			Error::UnexpectedToken(t, _) => format!("Hailet-man hinnatandus : {:?}", t),
			Error::Eof => "Faim du fishié hinnatandus".into(),
			Error::Undefined(n) => format!("Varillableu ou fonkssion heinkonu : {}", n),
			Error::InvalidType => "Tip heinvallide".into(),
			Error::InvalidCall(exp, act) => format!("Hapaile heinvallide : {} arguman olieux de {}", act, exp),
		})]);
	}
	esse_te_de::heinprimey(vec!["", "🌎 ½ 👃 (terremynez)", ""].into_iter().map(|x| Var::Str(x.into())).collect());
}

fn tok_parse_run(code: &str) -> Result<(), Error> {
	Executor::new(Ast::parse(tokenize(code).tokens)?).run()
}

fn tokenize(code: &str) -> Tokenizer {
	code.chars().fold(Tokenizer::new(code.len()), |mut t, c| {
		match t.gramm {
			Grammar::Comment => match c {
				'\n' => Tokenizer { gramm: Grammar::Main, ..t },
				_ => t,
			},
			Grammar::Str => if t.chars.last().map(|x| *x == '\\').unwrap_or(false) {
				match c {
					'n' => { t.chars.push('\n'); t },
					't' => { t.chars.push('\n'); t },
					'r' => { t.chars.push('\n'); t },
					'\\' => { t.chars.push('\n'); t },
					'"' => { t.chars.push('\n'); t },
					_ => panic!("Invalid escape sequence"),
				}
			} else {
				if c == '"' {
					t.tokens.push(Tok::Str(t.chars.iter().collect()));
					t.chars.clear();
					Tokenizer { gramm: Grammar::Main, ..t }
				} else {
					t.chars.push(c);
					t
				}
			},
			Grammar::Main => match c {
				' ' | '\t' | '\n' | '.' | '(' | ')' | ',' |
				':' | '=' | '[' | ']' | '{' | '}' => {
					let word: String = t.chars.iter().collect();
					if let Some(symb) = symbol(&word) {
						t.tokens.push(symb);
					} else {
						if let Ok(n) = i64::from_str(&word) {
							t.tokens.push(Tok::Number(n));
						} else if word.len() > 0 {
							t.tokens.push(Tok::Ident(word));
						}
					}
					t.chars.clear();

					if let Some(s) = symbol(&c.to_string()) {
						t.tokens.push(s);
					}

					t
				},
				'"' => Tokenizer { gramm: Grammar::Str, ..t },
				'#' => Tokenizer { gramm: Grammar::Comment, ..t },
				_ => { t.chars.push(c); t }
			}
		}
	})
}

fn symbol(s: &str) -> Option<Tok> {
	match s {
		"swa" => Some(Tok::Swa),
		"." => Some(Tok::Dot),
		"(" => Some(Tok::LPar),
		")" => Some(Tok::RPar),
		"," => Some(Tok::Comma),
		":" => Some(Tok::Colon),
		"=" => Some(Tok::Equal),
		"\n" => Some(Tok::NewLine),
		"fonkssion" => Some(Tok::Fonkssion),
		"tank" => Some(Tok::Tank),
		"euh" => Some(Tok::Euh),
		"çy" => Some(Tok::Cy),
		"c" => Some(Tok::C),
		"pa" => Some(Tok::Pa),
		"bon" => Some(Tok::Bon),
		"houx" => Some(Tok::Houx),
		"alore" => Some(Tok::Alore),
		"ranveauha" => Some(Tok::Ranveauha),
		"faim" => Some(Tok::Faim),
		"vré" => Some(Tok::Vre),
		"pho" => Some(Tok::Pho),
		"[" => Some(Tok::LBrace),
		"]" => Some(Tok::RBrace),
		"{" => Some(Tok::LAcc),
		"}" => Some(Tok::RAcc),
		_ => None,
	}
}

#[derive(Debug)]
enum Error {
	UnexpectedToken(Tok, Tok),
	Eof,
	Undefined(String),
	InvalidType,
	// (expected args, actual args)
	InvalidCall(usize, usize),
}

macro_rules! expect {
	($toks:ident, $n:expr, $tok:expr) => {
		if let Some(t) = $toks.iter().nth($n) {
			if *t == $tok {
				Ok(())
			} else {
				Err(Error::UnexpectedToken(t.clone(), $tok))
			}
		} else {
			Err(Error::Eof)
		}
	}
}

macro_rules! accept {
	($toks:ident, $n:expr, $tok:expr) => {
		if expect!($toks, $n, $tok).is_ok() {
			1
		} else {
			0
		}
	}
}

macro_rules! expect_ident {
	($toks:ident, $n:expr) => {
		if let Some(i) = $toks.iter().nth($n) {
			match i {
				Tok::Ident(i) => {
					Ok(i)
				},
				x => Err(Error::UnexpectedToken(x.clone(), Tok::Ident(String::new()))),
			}
		} else {
			Err(Error::Eof)
		}
	}
}

impl Ast {
	fn parse(toks: Vec<Tok>) -> Result<Ast, Error> {
		log::debug!("Tokens :\n{:#?}", &toks);
		let (_, ast) = Self::parse_block(&toks, |t| t.len() == 0)?;
		Ok(Ast::Root(ast))
	}

	fn parse_block(toks: &[Tok], stop: impl Copy + FnOnce(&[Tok]) -> bool) -> Result<(&[Tok], Vec<Ast>), Error> {
		let mut ast = vec![];
		let mut toks: &[Tok] = &toks;
		loop {
			if stop(&toks) {
				break;
			}

			while expect!(toks, 0, Tok::NewLine).is_ok() {
				toks = &toks[1..];
			}

			match Self::parse_decl(toks)
				.or_else(|_| Self::parse_assign(toks))
				.or_else(|_| Self::parse_ast_call(toks))
				.or_else(|_| Self::parse_func(toks))
				.or_else(|_| Self::parse_return(toks))
				.or_else(|_| Self::parse_if(toks))
				.or_else(|_| Self::parse_while(toks)) {
					Err(e) => return Err(e),
					Ok((t, a)) => { ast.push(a); toks = t; },
				}

			while expect!(toks, 0, Tok::NewLine).is_ok() {
				toks = &toks[1..];
			}
		}
		Ok((toks, ast))
	}

	fn parse_return(toks: &[Tok]) -> Result<(&[Tok], Ast), Error> {
		expect!(toks, 0, Tok::Ranveauha)?;
		let (t, e) = Self::parse_expr(&toks[1..])?;
		Ok((t, Ast::Return(e)))
	}

	fn parse_decl(toks: &[Tok]) -> Result<(&[Tok], Ast), Error> {
		expect!(toks, 0, Tok::Swa)?;
		let i = expect_ident!(toks, 1)?;
		expect!(toks, 2, Tok::Equal)?;

		match Self::parse_expr(&toks[3..]) {
			Ok((t, e)) => {
				expect!(t, 0, Tok::NewLine)?;
				Ok((&t[1..], Ast::Decl(i.to_string(), e)))
			},
			Err(x) => Err(x),
		}
	}

	fn parse_assign(toks: &[Tok]) -> Result<(&[Tok], Ast), Error> {
		let i = expect_ident!(toks, 0)?;
		expect!(toks, 1, Tok::Equal)?;

		match Self::parse_expr(&toks[2..]) {
			Ok((t, e)) => {
				expect!(t, 0, Tok::NewLine)?;
				Ok((&t[1..], Ast::Decl(i.to_string(), e)))
			},
			Err(x) => Err(x),
		}
	}

	fn parse_call(toks: &[Tok]) -> Result<(&[Tok], String, Vec<Expr>), Error> {
		let name = expect_ident!(toks, 0)?;
		expect!(toks, 1, Tok::LPar)?;
		let mut args = vec![];
		let mut toks = &toks[2..];
		if expect!(toks, 0, Tok::RPar).is_err() {
			loop {
				let (t, e) = Self::parse_expr(toks)?;
				args.push(e);

				if expect!(t, 0, Tok::Comma).is_ok() {
					toks = &t[1..];
				} else {
					toks = &t;
					break;
				}
			}
		}
		expect!(toks, 0, Tok::RPar)?;

		Ok((&toks[1..], name.to_string(), args))
	}

	fn parse_ast_call(toks: &[Tok]) -> Result<(&[Tok], Ast), Error> {
		let (t, name, args) = Self::parse_call(toks)?;
		expect!(t, 0, Tok::NewLine)?;
		Ok((&t[1..], Ast::Call(name, args)))
	}

	fn parse_func(toks: &[Tok]) -> Result<(&[Tok], Ast), Error> {
		expect!(toks, 0, Tok::Fonkssion)?;
		let i = expect_ident!(toks, 1)?;
		expect!(toks, 2, Tok::LPar)?;
		let mut args = vec![];
		let mut toks = &toks[3..];
		loop {
			if let Ok(i) = expect_ident!(toks, 0) {
				args.push(i.clone());

				if expect!(toks, 0, Tok::Comma).is_ok() {
					toks = &toks[2..];
				} else {
					toks = &toks[1..];
					break;
				}
			} else {
				break;
			}
		}
		expect!(toks, 0, Tok::RPar)?;
		expect!(toks, 1, Tok::Colon)?;
		expect!(toks, 2, Tok::Colon).ok();

		let (t, inst) = Self::parse_block(&toks[3..], |t| expect!(t, 0, Tok::Faim).is_ok())?;
		expect!(t, 0, Tok::Faim)?;
		Ok((&t[1..], Ast::Func(i.to_string(), args, inst)))
	}

	fn parse_if(toks: &[Tok]) -> Result<(&[Tok], Ast), Error> {
		expect!(toks, 0, Tok::Cy)?;
		let (toks, cond) = Self::parse_expr(&toks[1..])?;
		expect!(toks, 0, Tok::Colon)?;
		let pad = accept!(toks, 1, Tok::NewLine);
		let (t, inst) = Self::parse_block(
			&toks[(pad + 1)..],
			|t| t.iter().next().map(|t| *t == Tok::Cy || *t == Tok::Faim || *t == Tok::Houx).unwrap_or(true)
		)?;

		let mut toks = t;
		let mut cys = vec![
			(cond, inst)
		];
		loop {
			if expect!(toks, 0, Tok::Houx).is_ok() {
				expect!(toks, 1, Tok::Alore)?;
				expect!(toks, 2, Tok::Cy)?;
				let (e_toks, c) = Self::parse_expr(&toks[3..])?;
				expect!(e_toks, 0, Tok::Colon)?;
				let pad = accept!(e_toks, 1, Tok::NewLine);
				let (t, inst) = Self::parse_block(
					&e_toks[(pad + 1)..],
					|t| t.iter().next().map(|t| *t == Tok::Cy || *t == Tok::Faim || *t == Tok::Houx).unwrap_or(true)
				)?;
				toks = t;
				cys.push((c, inst));
			} else if expect!(toks, 0, Tok::Cy).is_ok() {
				expect!(toks, 1, Tok::C)?;
				expect!(toks, 2, Tok::Pa)?;
				expect!(toks, 3, Tok::Bon)?;
				expect!(toks, 4, Tok::Colon)?;
				let pad = accept!(toks, 5, Tok::NewLine);
				let (t, inst) = Self::parse_block(
					&toks[(pad + 5)..],
					|t| t.iter().next().map(|t| *t == Tok::Faim).unwrap_or(true)
				)?;
				toks = t;
				cys.push((Expr::Scalar(Scalar::Bool(true)), inst));
			} else {
				expect!(toks, 0, Tok::Faim)?;
				toks = &toks[1..];
				break;
			}
		}

		Ok((toks, Ast::If(cys)))
	}

	fn parse_while(toks: &[Tok]) -> Result<(&[Tok], Ast), Error> {
		expect!(toks, 0, Tok::Tank)?;
		expect!(toks, 1, Tok::Euh)?;
		let (toks, cond) = Self::parse_expr(&toks[2..])?;
		expect!(toks, 0, Tok::Colon)?;
		let pad = accept!(toks, 1, Tok::NewLine);
		let (toks, inst) = Self::parse_block(
			&toks[(pad + 1)..],
			|t| t.iter().next().map(|t| *t == Tok::Faim).unwrap_or(true)
		)?;
		expect!(toks, 0, Tok::Faim)?;
		Ok((&toks[1..], Ast::While(cond, inst)))
	}

	fn parse_expr(toks: &[Tok]) -> Result<(&[Tok], Expr), Error> {
		if let Ok((t, s)) = Self::parse_scalar(toks) {
			Ok((t, Expr::Scalar(s)))
		} else if let Ok((t, c)) = Self::parse_expr_call(toks) {
			Ok((t, c))
		} else if let Ok((t, v)) = Self::parse_list_access(toks) {
			Ok((t, v))
		} else {
			Self::parse_var(toks)
		}
	}

	fn parse_list_access(toks: &[Tok]) -> Result<(&[Tok], Expr), Error> {
		let (t, e) = Self::parse_var(&toks)?;
		expect!(t, 0, Tok::LBrace)?;
		let (t, ind) = Self::parse_expr(&t[1..])?;
		expect!(t, 0, Tok::RBrace)?;
		Ok((&t[1..], Expr::ListAccess(Box::new(e), Box::new(ind))))
	}

	fn parse_expr_call(toks: &[Tok]) -> Result<(&[Tok], Expr), Error> {
		let (t, name, args) = Self::parse_call(toks)?;
		Ok((t, Expr::Call(name, args)))
	}

	fn parse_var(toks: &[Tok]) -> Result<(&[Tok], Expr), Error> {
		Ok((&toks[1..], Expr::Var(expect_ident!(toks, 0)?.clone())))
	}

	fn parse_scalar(toks: &[Tok]) -> Result<(&[Tok], Scalar), Error> {
		Self::parse_float(toks)
			.or_else(|_| Self::parse_int(toks))
			.or_else(|_| Self::parse_str(toks))
			.or_else(|_| Self::parse_bool(toks))
			.or_else(|_| Self::parse_list(toks))
			.or_else(|_| Self::parse_dict(toks))
	}

	fn parse_dict(toks: &[Tok]) -> Result<(&[Tok], Scalar), Error> {
		expect!(toks, 0, Tok::LAcc)?;
		let mut elts = HashMap::new();
		let mut toks = &toks[1..];
		if expect!(toks, 0, Tok::RAcc).is_err() {
			loop {
				let i = expect_ident!(toks, 0)?;
				expect!(toks, 1, Tok::Colon)?;
				let (t, e) = Self::parse_expr(&toks[2..])?;
				elts.insert(i.clone(), e);

				if expect!(t, 0, Tok::Comma).is_ok() {
					toks = &t[1..];
				} else {
					toks = &t;
					break;
				}
			}
		}
		expect!(toks, 0, Tok::RAcc)?;
		Ok((&toks[1..], Scalar::Dict(elts)))
	}

	fn parse_list(toks: &[Tok]) -> Result<(&[Tok], Scalar), Error> {
		expect!(toks, 0, Tok::LBrace)?;
		let mut elts = vec![];
		let mut toks = &toks[1..];
		if expect!(toks, 0, Tok::RBrace).is_err() {
			loop {
				let (t, e) = Self::parse_expr(toks)?;
				elts.push(e);

				if expect!(t, 0, Tok::Comma).is_ok() {
					toks = &t[1..];
				} else {
					toks = &t;
					break;
				}
			}
		}
		expect!(toks, 0, Tok::RBrace)?;
		Ok((&toks[1..], Scalar::List(elts)))
	}

	fn parse_bool(toks: &[Tok]) -> Result<(&[Tok], Scalar), Error> {
		if expect!(toks, 0, Tok::Vre).is_ok() {
			Ok((&toks[1..], Scalar::Bool(true)))
		} else {
			expect!(toks, 0, Tok::Pho)?;
			Ok((&toks[1..], Scalar::Bool(false)))
		}
	}

	fn parse_str(toks: &[Tok]) -> Result<(&[Tok], Scalar), Error> {
		if let Some(s) = toks.iter().nth(0) {
			match s {
				Tok::Str(s) => Ok((&toks[1..], Scalar::Str(s.clone()))),
				x => Err(Error::UnexpectedToken(x.clone(), Tok::Str(String::new()))),
			}
		} else {
			Err(Error::Eof)
		}
	}

	fn parse_float(toks: &[Tok]) -> Result<(&[Tok], Scalar), Error> {
		if let Some(e) = toks.iter().next() {
			match e {
				Tok::Number(e) => {
					expect!(toks, 1, Tok::Dot)?;

					if let Some(d) = toks.iter().nth(2) {
						match d {
							Tok::Number(d) => {
								Ok((&toks[3..], Scalar::Float(f64::from_str(&format!("{}.{}", e, d)).unwrap())))
							},
							x => Err(Error::UnexpectedToken(x.clone(), Tok::Number(0))),
						}
					} else {
						Err(Error::Eof)
					}
				},
				x => Err(Error::UnexpectedToken(x.clone(), Tok::Number(0))),
			}
		} else {
			Err(Error::Eof)
		}
	}

	fn parse_int(toks: &[Tok]) -> Result<(&[Tok], Scalar), Error> {
		if let Some(i) = toks.iter().next() {
			match i {
				Tok::Number(n) => Ok((&toks[1..], Scalar::Int(*n))),
				x => Err(Error::UnexpectedToken(x.clone(), Tok::Number(0))),
			}
		} else {
			Err(Error::Eof)
		}
	}
}

type Vars = HashMap<String, Var>;

#[derive(Clone)]
pub enum Func {
	Native(fn(Vec<Var>) -> Var),
	Painez(Vec<String>, Vec<Ast>),
}

impl PartialEq for Func {
	fn eq(&self, other: &Self) -> bool {
        match self {
        	Func::Native(f1) => match other {
        		Func::Native(f2) => f1 == f2,
        		_ => false,
        	},
        	Func::Painez(a1, _) => match other {
        		Func::Painez(a2, _) => a1 == a2,
        		_ => false,
        	},
        }
    }
}

impl std::fmt::Debug for Func {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Fonkssion")
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum Var {
	Func(Func),
	Int(i64),
	Float(f64),
	Str(String),
	Bool(bool),
	List(Vec<Var>),
	Dict(HashMap<String, Var>),
	Void,
}

impl Var {
	fn str_repr(&self) -> String {
		let res = match self {
			Var::Func(f) => match f {
				Func::Native(_) => "<fonkssion nattiveux>".into(),
				Func::Painez(a, _) => format!("<fonkssion an painezlaup ki ha {} kom paramaitreux", a.join(", ")),
			},
			Var::Int(i) => format!("{}", i),
			Var::Float(f) => format!("{}", f),
			Var::Str(s) => format!("{}", s),
			Var::Bool(b) => if *b { "vré".into() } else { "Pho".into() },
			Var::List(l) => format!("[ {} ]", l.iter().map(Var::str_repr).collect::<Vec<_>>().join(", ")),
			Var::Dict(d) => format!("{{ {} }}", d.iter().map(|(k, v)| format!("{}: {}", k, v.str_repr())).collect::<Vec<_>>().join(", ")),
			Var::Void => "rihin".into(),
		};
		res
	}
}

struct Executor {
	ast: Ast,
	vars: Vars,
	returned_val: Var,
}

macro_rules! import_std {
	($( $painez_name:ident : $name:ident ),*) => {
		{
			let mut vars = HashMap::new();
			$( vars.insert(stringify!($painez_name).into(), Var::Func(Func::Native(esse_te_de::$name))); )*
			vars
		}
	}
}

impl Executor {
	fn new(ast: Ast) -> Executor {
		let v = import_std!(
			heinprimey: heinprimey,
			aigualitet: aigualitet,
			hainférilleur: hainferilleur,
			hageoutez: hageoutez,
			kaulé: kaule,
			keskecé: keskece,
			li_le_numaireau: li_le_numaireau,
			taye: taye,
			ferrand: ferrand,
			hainferilleur_houx_aigal: hainferilleur_houx_aigal,
			çu_perd_illeure: cu_perd_illeure,
			çu_perd_illeure_houx_aigal: cu_perd_illeure_houx_aigal,
			féranss: feranss,
			muletiplyer: muletiplyer,
			vizet: vizet,
			kaulavek: kaulavek,
			koup: koup,
			klai: klai
		);

		Executor {
			ast,
			vars: v,
			returned_val: Var::Void,
		}
	}

	fn run(mut self) -> Result<(), Error> {
		self.run_ast(&self.ast.clone()).map(|_| ())
	}

	fn run_ast(&mut self, ast: &Ast) -> Result<bool, Error> {
		match ast {
			Ast::Root(xs) => for x in xs {
				if self.run_ast(&x)? {
					break
				}
			},
			Ast::Decl(name, exp) => {
				let e = { self.run_expr(&exp)? };
				self.vars.insert(name.clone(), e);
			},
		    Ast::Call(func, args) => {
		    	if let Some(Var::Func(f)) = self.vars.clone().get(&func.clone()) {
		    		self.call_func(f.clone(), args.clone())?;
		    	} else {
		    		return Err(Error::Undefined(func.clone()));
		    	}
		    },
		    Ast::Func(name, args, inst) => {
		    	self.vars.insert(name.clone(), Var::Func(Func::Painez(args.clone(), inst.clone())));
		    },
		    Ast::Return(e) => {
		    	self.returned_val = self.run_expr(&e)?;
		    	return Ok(true);
		    },
		    Ast::If(branches) => {
		    	for (cond, inst) in branches {
		    		if let Var::Bool(b) = self.run_expr(&cond)? {
			    		if b {
			    			for x in inst {
			    				if self.run_ast(&x)? {
									break;
								}
			    			}
			    			break;
			    		}
			    	} else {
			    		return Err(Error::InvalidType);
			    	}
		    	}
		    },
		    Ast::While(cond, inst) => {
		    	loop {
		    		if let Var::Bool(b) = self.run_expr(&cond)? {
			    		if b {
			    			for x in inst {
			    				if self.run_ast(&x)? {
									break;
								}
			    			}
			    		} else {
			    			break;
			    		}
			    	} else {
			    		return Err(Error::InvalidType);
			    	}
		    	}
		    }
		}

		Ok(false)
	}

	fn run_expr(&mut self, expr: &Expr) -> Result<Var, Error> {
		match expr {
		    Expr::Scalar(s) => Ok(match s {
		    	Scalar::Bool(b) => Var::Bool(*b),
			    Scalar::Int(i) => Var::Int(*i),
			    Scalar::Float(f) => Var::Float(*f),
			    Scalar::Str(s) => Var::Str(s.clone()),
			    Scalar::List(l) => Var::List({
			    	let mut elts = vec![];
			    	for x in l {
			    		elts.push(self.run_expr(x)?);
			    	}
			    	elts
			    }),
			    Scalar::Dict(d) => Var::Dict({
			    	let mut elts = HashMap::new();
			    	for (k, v) in d {
			    		elts.insert(k.clone(), self.run_expr(v)?);
			    	}
			    	elts
			    }),
		    }),
		    Expr::Var(name) => if let Some(v) = self.vars.get(&name.clone()) {
		    	Ok(v.clone())
		    } else {
		    	Err(Error::Undefined(name.clone()))
		    },
		    Expr::Call(func, args) => if let Some(Var::Func(f)) = self.vars.clone().get(&func.clone()) {
	    		self.call_func(f.clone(), args.clone())
	    	} else {
	    		Err(Error::Undefined(func.to_string()))
	    	},
	    	Expr::ListAccess(expr, ind) => {
	    		let e = self.run_expr(expr)?;
	    		if let Var::List(l) = e {
	    			if let Var::Int(i) = self.run_expr(ind)? {
	    				if let Some(e) = l.iter().nth(i as usize) {
	    					Ok(e.clone())
	    				} else {
	    					Err(Error::Undefined("Haindice heinkorekt".into()))
	    				}
	  	    		} else {
	    				Err(Error::InvalidType)
	    			}
	    		} else if let Var::Dict(d) = e {
	    			if let Var::Str(k) = self.run_expr(ind)? {
	    				if let Some(e) = d.get(&k) {
	    					Ok(e.clone())
	    				} else {
	    					Err(Error::Undefined("Klet heinkorekt".into()))
	    				}
	  	    		} else {
	    				Err(Error::InvalidType)
	    			}
	    		} else {
	    			Err(Error::InvalidType)
	    		}
	    	}
		}
	}

	fn call_func(&mut self, func: Func, args: Vec<Expr>) -> Result<Var, Error> {
		match func {
			Func::Native(f) => {
				let mut a = vec![];
	    		for x in args {
	    			a.push(self.run_expr(&x)?);
	    		}
				Ok(f(a))
			},
			Func::Painez(params, inst) => {
				for (i, p) in params.iter().enumerate() {
					if let Some(v) = args.get(i) {
						let e = { self.run_expr(v)? };
						self.vars.insert(p.clone(), e);
					} else {
						return Err(Error::InvalidCall(params.len(), args.len()))
					}
				}
				self.returned_val = Var::Void;
				for x in inst {
    				if self.run_ast(&x)? {
						break
					}
    			}
				for p in params {
					self.vars.remove(&p);
				}
				Ok(self.returned_val.clone())
			}
		}
	}
}

#[cfg(test)]
#[test]
fn main_test() {
	let code =
r#"# Ça c'est un commentaire déjà

# Créer une nouvelle variable :
swa la_vie = 42
# Changer sa valeur :
la_vie = 12

# Les différents types en action :
# Entier :
swa i = 3
# Flottant :
swa f = 3.14
# String :
swa yo = "Salut la zone"
# Booléen :
swa on_dit_beurre_salé = vré
swa on_dit_pain_au_chocolat = pho

# Une fonction :
fonkssion haife(x):
    swa y = 0
    # Petite boucle while :
    tank euh hainférilleur(y, x):
        heinprimey(y)
        y = hageoutez(y, 1)
    faim

    # Les conditions
    çy aigualitet(x, 12): # Si / if
        ranveauha "cc clara"
    houx alore çy aigualitet(x, 42): # Sinon si / elif
        ranveauha "oui péné g la ref, mais non g pa lu le livre déso"
    çy c pa bon: # Sinon / else
        ranveauha "t ki??"
    faim
faim

swa maissaj = haife(la_vie)
heinprimey(maissaj)

swa legooms = ["patat douss", "farsidur", "kraipe", "kaméléon"]
swa i = 0
tank euh hainférilleur(i, taye(legooms)):
    heinprimey(legooms[i])
    i = hageoutez(i, 1)
faim
"#;

	tok_parse_run(code).unwrap();
}

